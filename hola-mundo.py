Name="hola-mundo"
Version="1"
Conflict=["hola-mundo"]
Maintainer="Hear505"
Contact="agoopazo@gmail.com"

Arch="all"

Description="""
Un test de paqueteria idur, con un hola mundo
"""

Install="""
echo "echo Hola Mundo" > /usr/bin/hola-mundo
chmod a+x /usr/bin/hola-mundo 
"""

Remove="""
rm -rf /usr/bin/hola-mundo
"""
